#/bin/bash

# Build script for image building on CI
#
# Parameter 'updater_image_separator' is used to allow pushing to GitLab container registry which uses / as separator

set -e

source "$(dirname "$0")/utils.sh"

app_image_name=$1
current_tag=$2
latest_tag=$3
image_type=$4
updater_image_separator="${5:-/}"

base_image="ghcr.io/dependabot-gitlab/dependabot-updater-$image_type:$(dependabot_version)"

if [ "$image_type" == "core" ]; then
  dockerfile="Dockerfile.core"
  image="$app_image_name"
else
  dockerfile="Dockerfile.ecosystem"
  image="${app_image_name}${updater_image_separator}${image_type}"
fi

images="$image:$current_tag,$image:$latest_tag"

log_with_header "Building image '$image:$current_tag'"
docker buildx build \
  --file "$dockerfile" \
  --platform="$BUILD_PLATFORM" \
  --provenance "false" \
  --build-arg COMMIT_SHA="$CI_COMMIT_SHA" \
  --build-arg PROJECT_URL="$CI_PROJECT_URL" \
  --build-arg VERSION="${CI_COMMIT_TAG:-$current_tag}" \
  --build-arg BASE_IMAGE="$base_image" \
  --cache-from type=registry,ref="$image:main-latest" \
  --cache-from type=registry,ref="$image:$latest_tag" \
  --cache-to type=inline \
  --output type=image,\"name="$images"\",push=true \
  .
