#!/bin/bash

set -eu -o pipefail

source "$(dirname "$0")/utils.sh"

release_version=$(cat app/services/version.rb | grep -oP 'VERSION = \K.*' | sed 's/"//g')

function run_migration() {
  bundle exec rake "db:migrate"
  bundle exec rake "background_tasks:run_background_migrations"
}

function seed_data() {
  bundle exec rake "db:seed"
}

log "** Fetching latest release 'v${release_version}' **"
git fetch origin tag "v${release_version}" --no-tags
git worktree add "release" "v${release_version}"

log "** Initializing database from latest release **"
(cd "release" && bundle install && run_migration && seed_data)
cp release/tmp/local_secret.txt tmp/local_secret.txt

log "** Running migrations **"
run_migration

log "** Validate no migrations are pending **"
bundle exec rake "background_tasks:check_migrations"

log "** Running seed data on migrated db **"
seed_data
