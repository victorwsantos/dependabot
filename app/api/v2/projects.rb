# frozen_string_literal: true

module V2
  class Projects < Grape::API
    include AuthenticationSetup
    include Pagination

    helpers do
      include APIHelpers

      def create_or_sync_project(project_name: project.name, access_token: nil)
        updated_project = Dependabot::Projects::Creator.call(project_name, access_token: access_token)
        Cron::JobSync.call(updated_project)

        updated_project
      end
    end

    namespace :projects do
      desc "Get all projects" do
        detail "Return array of all projects registered for dependency updates"
        success model: Project::Entity, examples: [Project::Entity.example_response], message: "Successful response"
        is_array true
      end
      params do
        use :pagination_params
      end
      get do
        present paginate(Project.all)
      end

      desc "Add project" do
        detail "Register project for dependency updates"
        success model: Project::Entity, examples: Project::Entity.example_response, message: "Successful response"
        failure [{ code: 409, message: "Project already exists" }]
      end
      params do
        requires :project_name, type: String, desc: "Project full path"
        optional :gitlab_access_token, type: String, desc: "Project specific access token"
      end
      post do
        project_name = params[:project_name]
        project = project(id: project_name)
        error!("Project already exists", 409) if project
      rescue Mongoid::Errors::DocumentNotFound
        log(:info, "Registering project '#{project_name}'")
        present create_or_sync_project(project_name: project_name, access_token: params[:gitlab_access_token])
      end

      route_param :id, type: String, desc: "Project id or url encoded full path" do
        desc "Get single project" do
          detail "Return single project"
          success model: Project::Entity, examples: Project::Entity.example_response, message: "Successful response"
          failure [{ code: 404, message: "Project not found" }]
        end
        get do
          present project
        end

        desc "Update project" do
          detail "Update project attributes"
          success model: Project::Entity, examples: Project::Entity.example_response, message: "Successful response"
          failure [{ code: 404, message: "Project not found" }]
        end
        params do
          optional :name, type: String, desc: "Project name"
          optional :forked_from_id, type: Integer, desc: "Forked from project id"
          optional :forked_from_name, type: String, desc: "Forked from project name"
          optional :webhook_id, type: Integer, desc: "Webhook id"
          optional :web_url, type: String, desc: "Web url"
          optional :configuration, type: Hash do
            optional :updates, type: Array[JSON], desc: "Updates configuration"
            optional :registries, type: JSON, desc: "Private registries configuration"
          end
        end
        put do
          project.update_attributes!(**params.slice(
            :name,
            :forked_from_id,
            :forked_from_name,
            :webhook_id,
            :web_url
          ))
          project.configuration.update_attributes!(**params[:configuration]) if params[:configuration]

          present project
        end

        desc "Remove project" do
          detail "Remove project from database"
          success message: "Removed project"
          failure [{ code: 404, message: "Project not found" }]
        end
        delete do
          Dependabot::Projects::Remover.call(project)
        end

        desc "Sync project" do
          detail "Sync project configuration with GitLab"
          success model: Project::Entity, examples: Project::Entity.example_response, message: "Successful response"
          failure [{ code: 404, message: "Project not found" }]
        end
        post :sync do
          present create_or_sync_project(project_name: project.name)
        end
      end
    end
  end
end
