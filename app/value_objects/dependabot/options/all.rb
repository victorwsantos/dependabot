# frozen_string_literal: true

require "yaml"

module Dependabot
  module Options
    # All options contained in dependabot configuration file
    #
    class All
      include ApplicationHelper
      include OptionsHelper

      # @param [String] config dependabot.yml configuration file
      # @param [String] project name of the project
      def initialize(config, project)
        @config = config
        @project = project
      end

      delegate :config_base_filename, to: DependabotConfig

      # Transform dependabot configuration file into options
      #
      # @return [Array<Hash>]
      def transform
        validate_config_options(DependabotConfigContract, merged_config)
        validate_config_options(UpdatesConfigContract, merged_config.slice(:updates))

        {
          registries: Registries.new(merged_config[:registries]).transform,
          updates: merged_config[:updates].map do |configuration|
            {
              fork: yml[:fork],
              **Main.new(project, configuration, yml[:registries]).transform,
              **Branch.new(configuration).transform,
              **CommitMessage.new(configuration).transform,
              **Rules.new(configuration).transform,
              **AutoMerge.new(configuration).transform,
              **Rebase.new(configuration).transform,
              **VulnerabilityAlerts.new(yml[:"vulnerability-alerts"], configuration).transform,
              **DependencyGroups.new(configuration).transform
            }.compact
          end
        }
      end

      private

      attr_reader :config, :project

      # Parsed dependabot yml config
      #
      # @return [Hash<Symbol, Object>]
      def yml
        @yml ||= YAML.safe_load(config, symbolize_names: true)
      end

      # Base configuration if exists
      #
      # @return [Hash]
      def base_config
        @base_config ||= begin
          return {} unless config_base_filename && File.exist?(config_base_filename)

          base = YAML.load_file(config_base_filename, symbolize_names: true)
          return {} unless base
          return base if base[:updates].nil? || base[:updates].is_a?(Hash)

          raise(ContractSchemaError, "`updates` key in base configuration `#{config_base_filename}` must be a map!")
        end
      end

      # Complete merged configuration
      #
      # @return [Hash]
      def merged_config
        @merged_config ||= base_config.deep_merge({
          **yml,
          updates: yml[:updates].map { |entry| (base_config[:updates] || {}).deep_merge(entry) }
        })
      end
    end
  end
end
