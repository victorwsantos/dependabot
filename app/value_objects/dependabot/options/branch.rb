# frozen_string_literal: true

module Dependabot
  module Options
    # Branch related updates options
    #
    class Branch < OptionsBase
      # Transform branch options
      #
      # @return [Hash]
      def transform
        {
          branch: opts[:"target-branch"],
          branch_name_prefix: opts.dig(:"pull-request-branch-name", :prefix) || "dependabot",
          branch_name_separator: opts.dig(:"pull-request-branch-name", :separator) || "-",
          branch_name_max_length: opts.dig(:"pull-request-branch-name", :"max-length")
        }
      end
    end
  end
end
