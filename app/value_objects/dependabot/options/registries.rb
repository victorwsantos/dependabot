# frozen_string_literal: true

module Dependabot
  module Options
    # Dependabot configuration registries options
    #
    class Registries # rubocop:disable Metrics/ClassLength
      include ApplicationHelper

      # Type mapping from yml values in config file to values accepted by dependabot core and supported contracts
      #
      # @return [Hash]
      TYPE_MAPPING = {
        "maven-repository" => {
          type: "maven_repository",
          url: "url",
          contracts: [PrivateRegistries::NoAuthContract, PrivateRegistries::PasswordContract]
        },
        "docker-registry" => {
          type: "docker_registry",
          url: "registry",
          contracts: [PrivateRegistries::NoAuthContract, PrivateRegistries::PasswordContract]
        },
        "npm-registry" => {
          type: "npm_registry",
          url: "registry",
          contracts: [
            PrivateRegistries::NoAuthContract,
            PrivateRegistries::TokenContract,
            PrivateRegistries::PasswordContract
          ]
        },
        "composer-repository" => {
          type: "composer_repository",
          url: "registry",
          contracts: [PrivateRegistries::NoAuthContract, PrivateRegistries::PasswordContract]
        },
        "git" => {
          type: "git_source",
          url: "host",
          contracts: [PrivateRegistries::NoAuthContract, PrivateRegistries::PasswordContract]
        },
        "nuget-feed" => {
          type: "nuget_feed",
          url: "url",
          contracts: [
            PrivateRegistries::NoAuthContract,
            PrivateRegistries::TokenContract,
            PrivateRegistries::PasswordContract
          ]
        },
        "python-index" => {
          type: "python_index",
          url: "index-url",
          contracts: [
            PrivateRegistries::NoAuthContract,
            PrivateRegistries::TokenContract,
            PrivateRegistries::PasswordContract
          ]
        },
        "rubygems-server" => {
          type: "rubygems_server",
          url: "host",
          contracts: [
            PrivateRegistries::NoAuthContract,
            PrivateRegistries::TokenContract,
            PrivateRegistries::PasswordContract
          ]
        },
        "terraform-registry" => {
          type: "terraform_registry",
          url: "host",
          contracts: [PrivateRegistries::NoAuthContract, PrivateRegistries::TokenContract]
        },
        "hex-organization" => {
          type: "hex_organization",
          contracts: [PrivateRegistries::KeyContract]
        },
        "hex-repository" => {
          type: "hex_repository",
          contracts: [PrivateRegistries::HexPrivateRepositoryContract]
        }
      }.freeze

      # Registries options
      #
      # @param [Hash] registries
      def initialize(registries)
        @registries = registries
      end

      # Fetch configured registries
      #
      # @return [Hash]
      def transform
        return {} unless registries

        registries
          .stringify_keys
          .transform_values { |registry| transform_registry_values(registry) }
          .compact
      end

      private

      # @return [Hash]
      attr_reader :registries

      # Update registry hash
      #
      # dependabot-core uses specific credentials hash depending on registry types with keys as strings
      # validate supported contracts and transform values to dependabot-core format unless all contracts fail
      #
      # @param [Hash] registry
      # @return [Hash]
      def transform_registry_values(registry)
        type = registry[:type]
        mapped_type = TYPE_MAPPING[type]&.except(:contracts)
        return warn_unsupported_registry(type) unless mapped_type

        result = config_validation_result(registry, TYPE_MAPPING[type][:contracts])
        return warn_incorrect_registry(type, result.reverse.find(&:failure?)) unless result.any?(&:success?)

        transformers.fetch(type, transformers["default"]).call(registry, mapped_type).compact
      end

      # Log warning for partially configured credentials
      #
      # @param [String] type
      # @param [Dry::Validation::Result] result
      # @return [nil]
      def warn_incorrect_registry(type, result)
        log(
          :error,
          "Detected schema errors for registry type '#{type}': [#{ContractSchemaError.format(result, '; ')}]"
        )
        nil
      end

      # Log warning for unsupported registry type
      #
      # @param [String] type
      # @return [nil]
      def warn_unsupported_registry(type)
        log(:error, "Registry type '#{type}' is not supported!")
        nil
      end

      # Check if registry credentials block is valid
      #
      # @param [Hash] registry
      # @param [Array<Dry::Validation::Contract>] contracts
      # @return [Array<Dry::Validation::Result>]
      def config_validation_result(registry, contracts)
        contracts.map { |contract| contract.new.call(registry) }
      end

      # Strip protocol from registries of specific type
      #
      # Private npm and docker registries will not work if protocol is defined
      #
      # @param [String] type
      # @param [String] url
      # @return [String]
      def strip_protocol(type, url)
        return url unless %w[npm-registry docker-registry terraform-registry].include?(type)

        url.gsub(%r{https?://}, "")
      end

      # :reek:DuplicateMethodCall

      # Registry value transformers
      #
      # @return [Hash]
      def transformers
        @transformers ||= {
          "default" => lambda do |registry, mapped_type|
            {
              "type" => mapped_type[:type],
              mapped_type[:url] => strip_protocol(registry[:type], registry[:url]),
              **registry.except(:type, :url).transform_keys(&:to_s)
            }
          end,
          "hex-organizations" => lambda do |registry, mapped_type|
            {
              "type" => mapped_type[:type],
              "organization" => registry[:organization],
              "token" => registry[:key]
            }
          end,
          "hex-repository" => lambda do |registry, mapped_type|
            {
              "type" => mapped_type[:type],
              **registry.except(:type).transform_keys { |key| key.to_s.tr("-", "_") }
            }
          end,
          "python-index" => lambda do |registry, mapped_type|
            token = registry[:token] || "#{registry[:username]}:#{registry[:password]}"

            {
              "type" => mapped_type[:type],
              "token" => token,
              "replaces-base" => registry[:"replaces-base"] || false,
              mapped_type[:url] => registry[:url]
            }
          end
        }
      end
    end
  end
end
