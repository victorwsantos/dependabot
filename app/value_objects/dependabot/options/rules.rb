# frozen_string_literal: true

module Dependabot
  module Options
    # Allow/ignore rule related options
    #
    class Rules < OptionsBase
      include OptionsHelper

      # Transform update allow/ignore rules options
      #
      # @return [Hash]
      def transform
        {
          # Allow all direct dependencies if not explicitly defined
          allow: transform_rule_options(opts[:allow]) || [{ dependency_type: "direct" }],
          ignore: transform_rule_options(opts[:ignore]) || []
        }
      end
    end
  end
end
