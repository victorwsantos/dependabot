terraform {
  backend "http" {}

  required_providers {

    google = {
      source  = "hashicorp/google"
      version = "~> 5.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.23.0"
    }

    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.3"
    }
  }
}

locals {
  namespace = "dependabot"
}

data "google_client_config" "default" {
}

data "google_container_cluster" "default" {
  name     = "dependabot"
  location = "us-central1"
}

provider "google" {
  project = "dependabot-gitlab"
  region  = "us-central1"
}

provider "kubernetes" {
  host                   = "https://${data.google_container_cluster.default.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(data.google_container_cluster.default.master_auth[0].cluster_ca_certificate)
}

provider "helm" {
  kubernetes {
    host                   = "https://${data.google_container_cluster.default.endpoint}"
    token                  = data.google_client_config.default.access_token
    cluster_ca_certificate = base64decode(data.google_container_cluster.default.master_auth[0].cluster_ca_certificate)
  }
}

resource "google_compute_global_address" "default" {
  name = "dependabot-static-ip"
}

resource "kubernetes_namespace" "default" {
  metadata {
    name = local.namespace
  }
}

resource "kubernetes_manifest" "backend_config" {
  manifest = {
    apiVersion = "cloud.google.com/v1"
    kind       = "BackendConfig"
    metadata = {
      name      = "backendconfig"
      namespace = local.namespace
    }

    spec = {
      healthCheck = {
        port        = 3000
        type        = "HTTP"
        requestPath = "/healthcheck"
      }
    }
  }

  depends_on = [
    kubernetes_namespace.default
  ]
}

resource "kubernetes_manifest" "managed_certs" {
  manifest = {
    apiVersion = "networking.gke.io/v1"
    kind       = "ManagedCertificate"
    metadata = {
      name      = "managed-cert"
      namespace = local.namespace
    }

    spec = {
      domains = [var.dependabot_host]
    }
  }

  depends_on = [
    kubernetes_namespace.default
  ]
}

resource "kubernetes_manifest" "frontend_config" {
  manifest = {
    apiVersion = "networking.gke.io/v1beta1"
    kind       = "FrontendConfig"
    metadata = {
      name      = "https-redirect"
      namespace = local.namespace
    }

    spec = {
      redirectToHttps = {
        enabled          = true
        responseCodeName = "PERMANENT_REDIRECT"
      }
    }
  }

  depends_on = [
    kubernetes_namespace.default
  ]
}


resource "helm_release" "dependabot" {
  name  = "dependabot-gitlab"
  chart = "https://storage.googleapis.com/dependabot-gitlab/dependabot-gitlab-pre.tgz"

  lint              = true
  atomic            = true
  wait              = true
  dependency_update = true
  create_namespace  = false

  namespace = local.namespace

  timeout = 300

  lifecycle {
    ignore_changes = [
      metadata
    ]
  }

  values = [
    yamlencode({
      image = {
        repository = var.image_registry
      }
    }),
    yamlencode({
      service = {
        type = "ClusterIP"
        annotations = {
          "cloud.google.com/neg"            = "{\"ingress\": true}"
          "cloud.google.com/backend-config" = "{\"default\": \"${kubernetes_manifest.backend_config.manifest.metadata.name}\"}"
        }
      }
    }),
    yamlencode({
      ingress = {
        enabled = true
        annotations = {
          "kubernetes.io/ingress.class"                 = "gce"
          "kubernetes.io/ingress.global-static-ip-name" = google_compute_global_address.default.name
          "networking.gke.io/managed-certificates"      = kubernetes_manifest.managed_certs.manifest.metadata.name
          "networking.gke.io/v1beta1.FrontendConfig"    = kubernetes_manifest.frontend_config.manifest.metadata.name
        }
        hosts = [
          {
            host = var.dependabot_host
            paths = [
              "/",
              "/favicon.ico",
              "/sign_in",
              "/logout",
              "/projects",
              "/projects/*",
              "/api/*",
              "/sidekiq/*",
              "/jobs/*",
              "/assets/*",
              "/metrics",
              "/new_project"
            ]
          }
        ]
      }
    }),
    yamlencode({
      auth = {
        enabled = true
      }
      credentials = {
        gitlab_access_token = sensitive(var.gitlab_access_token)
        github_access_token = sensitive(var.github_access_token)
        gitlab_auth_token   = sensitive(var.gitlab_hooks_auth_token)
        secretKeyBase       = sensitive(var.secret_key_base)
      }
      registriesCredentials = {
        GITLAB_DOCKER_REGISTRY_TOKEN = sensitive(var.gitlab_access_token)
        GITLAB_NPM_REGISTRY_TOKEN    = sensitive(var.gitlab_access_token)
        GITLAB_TF_REGISTRY_TOKEN     = sensitive(var.gitlab_access_token)
        GITLAB_PYPI_TOKEN            = sensitive(var.gitlab_access_token)
      }
    }),
    yamlencode({
      env = {
        sentryDsn                = sensitive(var.sentry_dsn)
        dependabotUrl            = "https://${var.dependabot_host}"
        mongoDbUri               = "mongodb+srv://${var.mongodb_username}:${sensitive(var.mongodb_password)}@${sensitive(var.mongodb_host)}/${var.mongodb_db_name}?retryWrites=true&w=majority&authSource=admin"
        redisUrl                 = sensitive(var.redis_url)
        redisTimeout             = 3
        commandsPrefix           = "@dependabot-bot"
        updateRetry              = false
        logColor                 = true
        logLevel                 = var.log_level
        sentryIgnoredErrors      = ["Redis::CannotConnectError", "Dependabot::PrivateSourceAuthenticationFailure"]
        expireRunData            = 604800
        sentryTracesSampleRate   = "1.0"
        sentryProfilesSampleRate = "1.0"
      }
    }),
    yamlencode({
      migrationJob = {
        activeDeadlineSeconds = 600
        enableCleanup         = true
        resources = {
          requests = {
            cpu    = "250m"
            memory = "512Mi"
          }
        }
      }
      backgroundTasksJob = {
        resources = {
          requests = {
            cpu    = "250m"
            memory = "512Mi"
          }
        }
      }
    }),
    yamlencode({
      project_registration = {
        mode           = "automatic"
        allow_pattern  = "dependabot-gitlab"
        ignore_pattern = "standalone"
      }
    }),
    yamlencode({
      worker = {
        maxConcurrency = 5
        updateStrategy = {
          type = "Recreate"
        }
        startupProbe = {
          initialDelaySeconds = 30
        }
        resources = {
          requests = {
            cpu    = "250m"
            memory = "768Mi"
          }
        }
        nodeSelector = {
          "cloud.google.com/gke-spot" = "true"
        }
      }
    }),
    yamlencode({
      web = {
        minConcurrency = 1
        maxConcurrency = 4
        startupProbe = {
          initialDelaySeconds = 30
        }
        resources = {
          requests = {
            cpu    = "250m"
            memory = "512Mi"
          }
        }
        nodeSelector = {
          "cloud.google.com/gke-spot" = "true"
        }
      }
    }),
    yamlencode({
      updater = {
        resources = {
          requests = {
            cpu    = 1
            memory = "512Mi"
          }
        }
        nodeSelector = {
          "cloud.google.com/gke-spot" = "true"
        }
      }
    }),
    yamlencode({
      redis = {
        enabled = false
        auth = {
          enabled  = true
          password = sensitive(var.redis_password)
        }
      }
      mongodb = {
        enabled = false
      }
    }),
    yamlencode({
      metrics = {
        enabled = true
      }
    })
  ]

  set {
    name  = "image.tag"
    value = var.image_tag
  }

  set {
    name  = "updater.imagePattern"
    value = "${var.image_registry}/%<package_ecosystem>s:${var.image_tag}"
  }

  depends_on = [
    google_compute_global_address.default,
    kubernetes_manifest.managed_certs,
    kubernetes_manifest.backend_config,
    kubernetes_manifest.frontend_config,
  ]
}
