# Common configuration

App supports base configuration template which can define common configuration options and merge them with project specific configuration options.

Path for base configuration file is configured via `SETTINGS__CONFIG_BASE_FILENAME` environment variable described in [configuration file](./environment.md#configuration-file) section.

Base configuration supports same values as the project specific `dependabot.yml` with one difference that `updates` key must define a map instead of an array. Project specific configuration would be merged on top of base configuration and options defined in `updates` of base configuration are merged with options of each `updates` entry in project specific configuration.

Project specific options will override base configuration options if option exists in both configuration files.
