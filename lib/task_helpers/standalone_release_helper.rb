# frozen_string_literal: true

require_relative "util"

class StandaloneReleaseHelper
  CI_FILE = ".gitlab-ci.yml"
  PROJECT = "dependabot-gitlab/dependabot-standalone"

  def initialize(version)
    @version = SemVer.parse(version).format(Util::VERSION_FORMAT_PATTERN)
    @branch = "main"
  end

  def self.call(version)
    new(version).update
  end

  def update
    logger.info("Updating dependabot-standalone image version")

    gitlab.edit_file(
      PROJECT,
      CI_FILE,
      branch,
      updated_gitlab_ci,
      "Update dependabot-gitlab version to #{version}\n\nchangelog: dependency"
    )
    gitlab.create_tag(PROJECT, version, "main")
  end

  private

  include Util

  attr_reader :version, :branch

  # gitlab-ci.yml file contents
  #
  # @return [String]
  def gitlab_ci
    @gitlab_ci ||= gitlab.file_contents(PROJECT, CI_FILE, branch)
  end

  # Version currently defined
  #
  # @return [String]
  def current_version
    @current_version ||= gitlab_ci.match(/DEPENDABOT_GITLAB_VERSION: (?<tag>\S+)/)[:tag]
  end

  # Updated .gitlab-ci.yml file
  #
  # @return [String]
  def updated_gitlab_ci
    @updated_gitlab_ci ||= gitlab_ci.gsub(current_version, version)
  end
end
