# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
namespace :background_tasks do
  desc "Run background tasks"
  task(run_post_deploy_tasks: :environment) do
    include ApplicationHelper

    errors = []
    tasks = [
      {
        message: "Running registration job creator task",
        task: -> { Dependabot::Projects::Registration::JobCreator.call }
      },
      {
        message: "Running vulnerability database sync job creator task",
        task: -> { Github::Vulnerabilities::UpdateJobCreator.call }
      },
      {
        message: "Running log entry expiry value update task",
        task: -> { LogEntryExpiryUpdater.call }
      },
      {
        message: "Schedule background migrations",
        task: -> { BackgroundMigrationJob.set(wait: 1.minute).perform_later }
      }
    ]

    tasks.each do |task|
      log(:info, "### #{task[:message]} ###")
      task[:task].call
      log(:info, "done!")
    rescue StandardError => e
      log_error(e)
      errors << e
    end

    exit(1) unless errors.empty?
  end

  desc "Update GitHub GraphQL schema"
  task(update_github_schema: :environment) do
    GraphQL::Client.dump_schema(Github::Graphql::HTTPAdapter, "db/schemas/github_schema.json")
  end

  desc "Update local vulnerability database"
  task(update_vulnerability_db: :environment) do
    SecurityVulnerabilityUpdateJob.perform_now
  end

  desc "Check db connection"
  task(check_db: :environment) do
    include ApplicationHelper

    Mongo::Logger.logger = Logger.new($stdout, level: :error)

    Mongoid
      .client(:default)
      .database_names
      .present?

    log(:info, "DB connection functional!")
  rescue StandardError => e
    log(:error, e.message)
    exit(1)
  end

  desc "Check redis connection"
  task(check_redis: :environment) do
    include ApplicationHelper

    RedisClient.config(**AppConfig.redis_config).new_client.with do |redis|
      redis.call("PING")
      redis.close
    end
    log(:info, "Redis connection functional!")
  rescue StandardError => e
    log(:error, e.message)
    exit(1)
  end

  desc "Check pending migrations"
  task(check_migrations: :environment) do
    include ApplicationHelper

    migrator = Mongoid::Migrator.new(:up, ["db/migrate"])
    pending_migrations = migrator.migrations.size - migrator.migrated.size
    raise("Migrations pending!") unless pending_migrations.zero?

    log(:info, "No migrations are pending!")
  rescue StandardError => e
    log(:error, e.message)
    exit(1)
  end

  desc "Run background migrations"
  task(run_background_migrations: :environment) do
    BackgroundMigrationJob.perform_now
  end
end

namespace :dependabot do
  desc "Update project dependencies"
  task(:update, %i[project package_ecosystem directory] => :environment) do |_task, args|
    include ApplicationHelper

    blank_keys = %i[project package_ecosystem directory].reject { |key| args[key] }
    raise(ArgumentError, "#{blank_keys} must not be blank") unless blank_keys.empty?

    errors = Job::Triggers::DependencyUpdate.call(args[:project], args[:package_ecosystem], args[:directory])
    next if errors.empty?

    log(:error, "Update run contained errors, marking as failed!")
    exit(1)
  rescue StandardError => e
    log_error(e)

    exit(1)
  end

  desc "Recreate merge request"
  task(:recreate_mr, %i[project mr_iid discussion_id] => :environment) do |_task, args|
    Job::Triggers::MergeRequestRecreate.call(args[:project], args[:mr_iid], args[:discussion_id])
  rescue StandardError => e
    log_error(e)

    exit(1)
  end

  desc "Trigger updates for specific dependency"
  task(:notify_release, %i[dependency_name package_ecosystem] => :environment) do |_task, args|
    include ApplicationHelper

    Job::Triggers::NotifyRelease.call(args[:dependency_name], args[:package_ecosystem])
  rescue StandardError => e
    log_error(e)

    exit(1)
  end

  desc "Register multiple projects for dependency updates"
  task(:register, [:projects] => :environment) do |_task, args|
    include ApplicationHelper

    args[:projects].split(" ").each do |project_name|
      log(:info, "Registering project '#{project_name}'")
      Dependabot::Projects::Creator.call(project_name).tap { |project| Cron::JobSync.call(project) }
    end
  end

  desc "Register single project for dependency updates with specific gitlab access token"
  task(:register_project, %i[project_name access_token] => :environment) do |_task, args|
    include ApplicationHelper

    project_name = args[:project_name]
    access_token = args[:access_token]

    log(:info, "Registering project '#{project_name}'")
    project = Dependabot::Projects::Creator.call(project_name, access_token: access_token)
    Cron::JobSync.call(project)
  end

  desc "Run automatic project registration"
  task(automatic_registration: :environment) do
    ProjectRegistrationJob.perform_now
  end

  desc "Remove dependency updates for project"
  task(:remove, [:project] => :environment) do |_task, args|
    Dependabot::Projects::Remover.call(args[:project])
  end

  desc "Validate config file"
  task(:validate, [:project] => :environment) do |_task, args|
    include ApplicationHelper

    log(:info, "Validating config '#{DependabotConfig.config_filename}'")
    Dependabot::Config::Fetcher.call(args[:project], update_cache: true)

    log(:info, "Configuration is valid")
  rescue ContractSchemaError => e
    log(:error, "Configuration not valid: #{e}")
    exit(1)
  end

  desc "Create user"
  task(:create_user, %i[username password] => :environment) do |_task, args|
    include ApplicationHelper

    username, password = args.values_at(:username, :password)
    raise("username and password arguments required") unless username && password

    User.create!(username: username, password: password)
    log(:info, "Successfully created user '#{username}'")
  rescue Mongoid::Errors::Validations => e
    log(:error, e.summary)
  end

  desc "Delete user"
  task(:delete_user, [:username] => :environment) do |_task, args|
    include ApplicationHelper

    username = args[:username]
    raise("username argument required") unless username

    User.find_by(username: args[:username]).delete
    log(:info, "Successfully delete user '#{username}'")
  rescue Mongoid::Errors::DocumentNotFound
    log(:error, "User with username: #{args[:username]} not found!")
  end
end
# rubocop:enable Metrics/BlockLength
