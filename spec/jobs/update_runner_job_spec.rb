# frozen_string_literal: true

describe UpdateRunnerJob, :integration, type: :job do
  subject(:job) { described_class }

  let(:project) { create(:project) }
  let(:package_ecosystem) { "bundler" }

  let(:args) do
    {
      "project_name" => project.name,
      "package_ecosystem" => package_ecosystem,
      "directory" => "/"
    }
  end

  before do
    allow(Job::Routers::DependencyUpdate).to receive(:call)
  end

  it { is_expected.to be_retryable UpdaterConfig.job_retries }

  it "queues job on default queue" do
    expect { job.perform_later(args) }.to enqueue_sidekiq_job
      .with(args)
      .on("default")
  end

  it "performs enqued job and saves last enqued time", :aggregate_failures do
    job.perform_now(args)

    expect(Job::Routers::DependencyUpdate).to have_received(:call).with(*args.values)
  end
end
