# frozen_string_literal: true

describe Dependabot::Config::Fetcher do
  subject(:fetched_config) { described_class.call(project) }

  include_context "with dependabot helper"

  let(:project) { "project" }
  let(:default_branch) { "main" }
  let(:config_file) { DependabotConfig.config_filename }
  let(:config) { Configuration.new(updates: updates_config, registries: registries) }

  let(:gitlab) do
    instance_double("Gitlab::Client",
                    project: Gitlab::ObjectifiedHash.new(default_branch: default_branch),
                    file_contents: raw_config)
  end

  before do
    allow(Gitlab::ClientWithRetry).to receive(:current) { gitlab }
  end

  context "without custom branch configuration" do
    it "fetches config from default branch" do
      expect(fetched_config).to eq(config)
      expect(gitlab).to have_received(:file_contents).with(project, config_file, default_branch)
    end
  end

  context "with custom branch configuration" do
    let(:branch) { "custom_branch" }

    before do
      allow(DependabotConfig).to receive(:config_branch) { branch }
    end

    it "fetches config from configured branch" do
      expect(fetched_config).to eq(config)
      expect(gitlab).to have_received(:file_contents).with(project, config_file, branch)
    end
  end
end
