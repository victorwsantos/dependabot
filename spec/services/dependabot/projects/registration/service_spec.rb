# frozen_string_literal: true

shared_examples "project registration skipped" do
  it "registration skipped", :aggregate_failures do
    sync.call

    expect(Dependabot::Projects::Creator).not_to have_received(:call)
    expect(Cron::JobSync).not_to have_received(:call).with(project)
  end
end

shared_examples "project registers" do
  it "registration performed", :aggregate_failures do
    sync.call

    expect(Dependabot::Projects::Creator).to have_received(:call).with(project.name, config: project.configuration)
    expect(Cron::JobSync).to have_received(:call).with(project)
  end
end

shared_examples "project removed" do
  it "removes project" do
    sync.call

    expect(Dependabot::Projects::Remover).to have_received(:call).with(project)
  end
end

describe Dependabot::Projects::Registration::Service, :integration do
  subject(:sync) { described_class }

  let(:gitlab) { instance_double("Gitlab::Client", projects: projects_response) }
  let(:projects_response) { instance_double("Gitlab::PaginatedResponse") }

  let(:config_yaml) do
    <<~YAML
      version: 2
      updates:
        - package-ecosystem: bundler
          directory: "/"
          schedule:
            interval: daily
            time: "01:00"
        - package-ecosystem: npm
          directory: "/"
          schedule:
            interval: daily
            time: "01:00"
    YAML
  end

  let(:cron) { "00 01 * * * UTC" }
  let(:default_branch) { "main" }
  let(:archived) { false }

  let(:project) { build(:project, config_yaml: config_yaml) }

  let(:gitlab_project) do
    Gitlab::ObjectifiedHash.new(
      id: project.id,
      path_with_namespace: project.name,
      name: project.name,
      default_branch: default_branch,
      archived: archived
    )
  end

  let(:jobs) do
    [
      Sidekiq::Cron::Job.new(name: "#{project.name}:bundler:/", cron: cron, class: UpdateRunnerJob.to_s),
      Sidekiq::Cron::Job.new(name: "#{project.name}:npm:/", cron: cron, class: UpdateRunnerJob.to_s)
    ]
  end

  before do
    allow(Gitlab::ClientWithRetry).to receive(:current) { gitlab }
    allow(projects_response).to receive(:auto_paginate).and_yield(gitlab_project)
    allow(Cron::JobSync).to receive(:call).with(project)

    allow(Dependabot::Projects::Creator).to receive(:call)
      .with(gitlab_project.path_with_namespace, config: kind_of(Configuration))
      .and_return(project)

    allow(Dependabot::Config::Fetcher).to receive(:call)
      .with(
        gitlab_project.path_with_namespace,
        branch: gitlab_project.default_branch,
        update_cache: true
      )
      .and_return(project.configuration)
  end

  context "with allowed project pattern not matching" do
    before do
      allow(AppConfig).to receive(:project_registration_allow_pattern).and_return("test")
    end

    it_behaves_like "project registration skipped"
  end

  context "with allowed project pattern matching" do
    before do
      allow(AppConfig).to receive(:project_registration_allow_pattern).and_return(project.name)
    end

    it_behaves_like "project registers"
  end

  context "with ignored project pattern matching" do
    before do
      allow(AppConfig).to receive(:project_registration_allow_pattern).and_return(project.name)
      allow(AppConfig).to receive(:project_registration_ignore_pattern).and_return(project.name)
    end

    it_behaves_like "project registration skipped"
  end

  context "with non existing project" do
    context "without configuration" do
      before do
        allow(Dependabot::Config::Fetcher).to receive(:call).and_raise(Dependabot::Config::MissingConfigurationError)
      end

      it_behaves_like "project registration skipped"
    end

    context "with configuration" do
      it_behaves_like "project registers"
    end

    context "without default_branch" do
      let(:default_branch) { nil }

      it_behaves_like "project registration skipped"
    end

    context "with archived project" do
      let(:archived) { true }

      it_behaves_like "project registration skipped"
    end
  end

  context "with existing project" do
    let(:project) { create(:project, config_yaml: config_yaml) }

    context "without config" do
      before do
        allow(Dependabot::Config::Fetcher).to receive(:call).and_raise(Dependabot::Config::MissingConfigurationError)
        allow(Dependabot::Projects::Remover).to receive(:call)
      end

      it_behaves_like "project removed"
    end

    context "with out of sync jobs", :aggregate_failures do
      before do
        allow(Sidekiq::Cron::Job).to receive(:all).and_return([jobs[0]])
      end

      it "syncs project and jobs" do
        sync.call

        expect(Dependabot::Projects::Creator).to have_received(:call).with(project.name, config: project.configuration)
        expect(Cron::JobSync).to have_received(:call).with(project)
      end
    end

    context "with jobs in sync", :aggregate_failures do
      before do
        allow(Sidekiq::Cron::Job).to receive(:all).and_return(jobs)
      end

      it_behaves_like "project registration skipped"
    end

    context "with renamed project", :aggregate_failures do
      let!(:old_name) { project.name }
      let!(:new_name) { Faker::Alphanumeric.unique.alpha(number: 15) }

      let(:gitlab_project) do
        Gitlab::ObjectifiedHash.new(
          id: project.id,
          name: project.name,
          path_with_namespace: new_name,
          default_branch: default_branch,
          archived: archived
        )
      end

      before do
        allow(Cron::JobRemover).to receive(:call)
      end

      it "renames existing project" do
        sync.call

        expect(project.reload.name).to eq(new_name)
        expect(Cron::JobRemover).to have_received(:call).with(old_name)
        expect(Cron::JobSync).to have_received(:call).with(project)
      end
    end

    context "with archived project" do
      let(:archived) { true }

      before do
        allow(Dependabot::Projects::Remover).to receive(:call)
      end

      it_behaves_like "project removed"
    end

    context "with out of sync configuration" do
      let(:updated_config) do
        project.configuration.clone.tap do |conf|
          conf.registries = Dependabot::Options::Registries.new({
            maven: {
              type: "maven-repository",
              url: "https://gitlab.com/api/v4/projects/1/packages/maven"
            }
          }).transform
        end
      end

      before do
        allow(Sidekiq::Cron::Job).to receive(:all).and_return(jobs)
        allow(Dependabot::Config::Fetcher).to receive(:call).and_return(updated_config)
      end

      it "syncs project and jobs" do
        sync.call

        expect(Dependabot::Projects::Creator).to have_received(:call).with(project.name, config: updated_config)
        expect(Cron::JobSync).to have_received(:call).with(project)
      end
    end
  end
end
