# frozen_string_literal: true

describe Job::Routers::MergeRequest::Update, :integration do
  subject(:update) do
    described_class.call(
      project_name: project.name,
      mr_iid: mr.iid
    )
  end

  include_context "with dependabot helper"

  let(:gitlab) { instance_double("Gitlab::Client", merge_request: gitlab_mr, rebase_merge_request: nil) }
  let(:container_runner) { Container::Compose::Runner }

  let(:config_yaml) do
    <<~YAML
      version: 2
      registries:
        dockerhub:
          type: docker-registry
          url: registry.hub.docker.com
          username: octocat
          password: password
      updates:
        - package-ecosystem: bundler
          directory: "/"
          schedule:
            interval: weekly
    YAML
  end

  let(:project) do
    create(
      :project_with_mr,
      config_yaml: config_yaml,
      dependency: "config",
      commit_message: "original-commit",
      branch: branch
    )
  end

  let(:mr) { project.merge_requests.first }
  let(:state) { "opened" }
  let(:branch) { "update-branch" }
  let(:assignee) { "dependabot" }
  let(:conflicts) { false }
  let(:discussion_id) { nil }

  let(:gitlab_mr) do
    Gitlab::ObjectifiedHash.new({
      "iid" => mr.iid,
      "state" => state,
      "web_url" => "url",
      "has_conflicts" => conflicts,
      "assignee" => { "username" => "dependabot" }
    })
  end

  def expect_mr_recreated
    expect(gitlab).not_to have_received(:rebase_merge_request)
    expect(container_runner).to have_received(:call).with(
      package_ecosystem: mr.package_ecosystem,
      task_name: "recreate_mr",
      task_args: [project.name, mr.iid]
    )
  end

  def expect_mr_skipped
    expect(gitlab).not_to have_received(:rebase_merge_request)
    expect(container_runner).not_to have_received(:call)
  end

  def expect_mr_rebased
    expect(gitlab).to have_received(:rebase_merge_request).with(project.name, mr.iid)
    expect(container_runner).not_to have_received(:call)
  end

  before do
    allow(Gitlab::ClientWithRetry).to receive(:current) { gitlab }
    allow(container_runner).to receive(:call)
  end

  around do |example|
    with_env("SETTINGS__DEPLOY_MODE" => "compose") { example.run }
  end

  context "with successfull update", :aggregate_failures do
    before { update }

    context "without recreate and conflicts" do
      it("rebases mr") { expect_mr_rebased }
    end

    context "with merge request conflicts" do
      let(:conflicts) { true }

      it("recreates mr") { expect_mr_recreated }
    end

    context "with correct assignee" do
      let(:config_yaml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: weekly
              rebase-strategy:
                with-assignee: dependabot
        YAML
      end

      it("rebases mr") { expect_mr_rebased }
    end
  end

  context "with unsuccessfull update" do
    let(:conflicts) { true }

    before { update }

    context "with merge request already merged" do
      let(:state) { "merged" }

      it("skips mr") { expect_mr_skipped }
    end

    context "with different assignee" do
      let(:config_yaml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: weekly
              rebase-strategy:
                with-assignee: test-user1
        YAML
      end

      it("skips mr") { expect_mr_skipped }
    end
  end
end
